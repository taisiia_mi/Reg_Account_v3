import Account.RegistrationUkranianPage;
import Other.CheckPaymentPage;
import Other.OrderProcPage;
import Account.RegistrationPage;
import Utils.*;
import com.sun.jna.platform.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by Home on 12.10.2017.
 */
public class RegistrationTest {
  static WebDriver driver;
  private static MyWaits wait;
  //takeSnapShot(driver, "c://test.png") ;
  @BeforeClass
  void setup() {
    //System.setProperty("webdriver.chrome.driver", "C:\\Users\\Yuliya\\Desktop\\setup\\chromedriver.exe");
    System.setProperty("webdriver.chrome.driver", "C:\\Users\\Home\\Downloads\\chromedriver.exe");
    // System.setProperty("webdriver.chrome.driver", "C:\\Users\\it-school\\Desktop\\setup\\chromedriver.exe");
    driver = (WebDriver) new ChromeDriver();
    // driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.chrome());
    driver.get("https://www.templatemonster.com/");
    driver.manage().window().maximize();
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    wait = new MyWaits(driver);
  }
  @BeforeMethod
  void buy() {
    OrderProcPage buypr = PageFactory.initElements(driver, OrderProcPage.class);
    buypr.chekout();
  }
//  @Test(dataProvider = "data-provider", dataProviderClass = DataPrForRegistration.class)
//  void registrationTest(String path){
//    RegistrationPage reg = PageFactory.initElements(driver, RegistrationPage.class);
//    CheckPaymentPage checkpayment=PageFactory.initElements(driver, CheckPaymentPage.class);
//    User user=new User(path);
//    reg.registerAccount(user);
//    wait.waitPayment();
//    Assert.assertTrue(checkpayment.ChechPaymentMethod());
//  }
  @Test
  void registrationTest(){
    RegistrationPage reg = PageFactory.initElements(driver, RegistrationPage.class);
    CheckPaymentPage checkpayment=PageFactory.initElements(driver, CheckPaymentPage.class);
    User user=new User("src/test/resources/UkrainianUser.properties");
    reg.registerAccount(user);
    wait.waitPayment();
    Assert.assertTrue(checkpayment.ChechPaymentMethod());
  }

  @AfterMethod
  public static void takeSnapShot(WebDriver webdriver,String fileWithPath) throws Exception{
    //Convert web driver object to TakeScreenshot
    TakesScreenshot scrShot =((TakesScreenshot)webdriver);
    //Call getScreenshotAs method to create image file
    File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
    //Move image file to new destination
    File DestFile=new File(fileWithPath);
    //Copy file at destination
    //FileUtils.copyFile(SrcFile, DestFile);

  }
  @AfterClass
  public void tearDown() {
  driver.manage().deleteAllCookies();driver.quit();
  }
  }

