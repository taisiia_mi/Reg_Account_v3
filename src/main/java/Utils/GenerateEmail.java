package Utils;

import java.util.Random;

/**
 * Created by Home on 14.10.2017.
 */
public class GenerateEmail {
  public String generateEmail() {
    Random random = new Random();
    String symbols = "1234567890abcdefghijklmnopqrstuvwxyz";
    int pos = random.nextInt(symbols.length());
    StringBuilder randString = new StringBuilder();
    for (int i = 0; i < 8; i++)
      randString.append(symbols.charAt((int) (Math.random() * symbols.length())));
    String email=randString+"@gmail.com";
  return email;
  }
}
