package Utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MyWaits {
    WebDriver driver;
    String language;
    public MyWaits(WebDriver driver) {
        this.driver = driver;
    }
    public void waitCookies (String cookieName){
        WebDriverWait wait = new WebDriverWait(driver,15);
        wait.until((WebDriver driver) -> driver.manage().getCookieNamed(cookieName)!=null);
    }
    public void waitClickHeart () {
        WebDriverWait wait = new WebDriverWait(driver, 15);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='menu-favorites']/b")));
    }
    public void waitClickLanguage () {
        WebDriverWait wait = new WebDriverWait(driver,15);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("menu-"+language+"-locale")));
    }
    public void waitClickLogout() {
        WebDriverWait wait = new WebDriverWait(driver,15);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='app-account-menu']/div/div/ul/li[5]/button")));
    }
    public void waitPopup() {
        WebDriverWait wait = new WebDriverWait(driver,15);
        wait.until(ExpectedConditions.invisibilityOfElementWithText(By.className("action js-activate-editing"),"Cart Summary"));
        //wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@class='\"modal-header\"")));
    }
    public void waitProduct() {
        WebDriverWait wait = new WebDriverWait(driver,15);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(" .//*[@id='products']/li[1]")));
    }
    public void waitCheckout() {
        WebDriverWait wait = new WebDriverWait(driver,15);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("cart-summary-checkout")));
    }

    public void waitElementClickable(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver,15);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }
    public void waitEmail() {
        WebDriverWait wait = new WebDriverWait(driver,60);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[contains(@id,'signin3-form-email')]")));
    }
    public void waitPayment (){
        WebDriverWait wait = new WebDriverWait(driver,15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='payment-methods-container']/div[1]")));
    }
    public void waitregForm() {
        WebDriverWait wait = new WebDriverWait(driver,15);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[contains(@id,'collapse-information')]")));
    }
  public void waitZipCodeVal () {
    WebDriverWait wait = new WebDriverWait(driver, 15);
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[contains(@class,'tm-icon icon-status')]")));


  }
}
