package Utils;
import java.io.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Random;
import java.util.Scanner;
import java.io.*;
import java.util.Properties;
/**
 * Created by Home on 12.10.2017.
 */
public class User {
  Properties property = new Properties();
    private  String name;
    private String country;
    private String city;
    private String phone1;
    private String phone2;
    private String zipcode;
    private String state;


    public User(String path) {
        InputStream fileInputStream=null;
        try {
            fileInputStream = new FileInputStream(path);
            property.load(fileInputStream);
        } catch (IOException e) {
            System.err.println("Error: File is absent!");
        } finally {
            if (fileInputStream != null)
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }


    public String getName() {
      name = property.getProperty("name");
      return name;
    }

    public String getCountry() {
      country = property.getProperty("country");
      return country;
    }

    public String getCity() {
      city = property.getProperty("city");
      return city;
    }

    public String getPhone1() {
      phone1 = property.getProperty("phonenumber1");
      return phone1;
    }

    public String getPhone2() {
      phone2 = property.getProperty("phonenumber2");
      return phone2;
    }

    public String getZipcode() {
      zipcode = property.getProperty("zipcode");
      return zipcode;
    }

    public String getState() {
      state = property.getProperty("state");
      return state;
    }

    public String generateEmail() {
        Random random = new Random();
        String symbols = "1234567890abcdefghijklmnopqrstuvwxyz";
        int pos = random.nextInt(symbols.length());
        StringBuilder randString = new StringBuilder();
        for (int i = 0; i < 8; i++)
            randString.append(symbols.charAt((int) (Math.random() * symbols.length())));
        String email=randString+"@gmail.com";
        return email;
    }

}