package Other;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Home on 18.10.2017.
 */
public class CheckPaymentPage {

  private final WebDriver driver;
  public CheckPaymentPage(WebDriver driver) {
    this.driver = driver;
  }
  @FindBy(xpath = ".//*[@id='payment-methods-container']/div[1]")
  WebElement paymentblock;
  public boolean ChechPaymentMethod() {
    return paymentblock.isDisplayed();
  }
}