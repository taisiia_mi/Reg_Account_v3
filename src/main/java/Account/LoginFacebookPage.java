package Account;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public class LoginFacebookPage {
    private final WebDriver driver;
    public LoginFacebookPage(WebDriver driver) {
        this.driver = driver;
    }
    @FindBy(id = "menu-signin-block")
    WebElement accountButton;

    @FindBy(id = "email")
    WebElement emailInput;

    @FindBy(id = "pass")
    WebElement passInput;

    @FindBy(id = "loginbutton")
    WebElement submitButton;

    @FindBy(id = "id-general-facebook-button")
    WebElement buttonFacebookAccount;


    public void loginFacebook(String email, String pass) {
        accountButton.click();
        driver.get("https://account.templatemonster.com/auth/#/");
        buttonFacebookAccount.click();

        List<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
        System.out.println(tabs2);
        driver.switchTo().window(tabs2.get(1));

        emailInput.sendKeys(email);
        passInput.sendKeys(pass);
        submitButton.click();
        driver.switchTo().window(tabs2.get(0));
    }


    public boolean isLogged(){
        return driver.findElement(By.id("top-panel-to-profile-page-link")).isDisplayed();
    }
}