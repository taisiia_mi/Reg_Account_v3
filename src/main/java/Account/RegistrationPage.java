package Account;
import Utils.Click;
import Utils.GenerateEmail;
import Utils.MyWaits;
import Utils.User;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Home on 12.10.2017.
 */
public class RegistrationPage {
  private final WebDriver driver;
  public RegistrationPage(WebDriver driver) {
    this.driver = driver;
  }
  @FindBy(id = "billinginfo3-form-fullname")
  WebElement namefield;
  @FindBy(xpath = ".//*[@id='billinginfo3_form_countryiso2_chosen']/a")
  WebElement countryfield;

  @FindBy(xpath = ".//*[@placeholder='Start enter country']")
  WebElement countryinput;

  @FindBy(xpath = ".//*[@id='billinginfo3_form_phone_code_chosen']/a/div")
  WebElement dropdownphone;

  @FindBy(xpath =".//*[@id='billinginfo3_form_phone_code_chosen']/div/div/input")
  WebElement phoneinput;

  @FindBy(id = "billinginfo3-form-phone")
  WebElement phonesecondfield;

  @FindBy(id = "billinginfo3-form-postalcode")
  WebElement zipcodefield;

  @FindBy(xpath = ".//*[@id='billinginfo3_form_stateiso2_chosen']/a/div")
  WebElement statefield;

  @FindBy(id = "billinginfo3-form-cityname")
  WebElement cityfield;

  @FindBy(xpath=".//*[contains(@class,'signin-switch-account')]")
  WebElement swithaccount;

  @FindBy(xpath = ".//*[contains(@id,'signin3-form-email')]")
  WebElement emailfield;

  @FindBy(id ="signin3-new-customer")
  WebElement continuebuton;

  @FindBy(id ="billinginfo3-form-submit")
  WebElement submitbutton;

  public void registerAccount(User user) {
    Click click=new Click(driver);
    GenerateEmail generateEmail=new GenerateEmail();
    MyWaits myWaits = new MyWaits(driver);
    myWaits.waitEmail();
       if (swithaccount.isDisplayed()) {
      swithaccount.click();
    }
    emailfield.sendKeys(generateEmail.generateEmail());
    continuebuton.click();
    myWaits.waitregForm();
    click.methodClick(namefield);
    namefield.sendKeys(user.getName());
    click.methodClick(countryfield);
    click.methodClick(countryinput);
    countryinput.sendKeys(user.getCountry());
    dropdownphone.click();
    click.methodClick(phoneinput);
    phoneinput.sendKeys(user.getPhone1());
    click.methodClick(phonesecondfield);
    phonesecondfield.sendKeys(user.getPhone2());
    click.methodClick(zipcodefield);
    zipcodefield.sendKeys(user.getZipcode());
    zipcodefield.sendKeys(Keys.TAB);
    if (countryfield.getText().equals("United States")){
      click.methodClick(statefield);
    statefield.sendKeys(user.getState());
    }
//    myWaits.waitZipCodeVal();
    click.methodClick(cityfield);
    cityfield.sendKeys(user.getCity());
    //click.methodClick(submitbutton);
    submitbutton.click();
}}
