package Account;
import Utils.GenerateEmail;
import Utils.MyWaits;
import Utils.Userold;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Home on 12.10.2017.
 */
public class RegistrationUkranianPage {
    private final WebDriver driver;
     public RegistrationUkranianPage(WebDriver driver) {
        this.driver = driver;
    }
    @FindBy(id = "billinginfo3-form-fullname")
    WebElement namefield;

    @FindBy(xpath = ".//*[@id='billinginfo3_form_countryiso2_chosen']/a")
    WebElement countryfield;

    @FindBy(xpath = ".//*[@placeholder='Start enter country']")
    WebElement countryinput;

    @FindBy(xpath = ".//*[@id='billinginfo3_form_phone_code_chosen']/a/div")
    WebElement dropdownphone;

    @FindBy(xpath =".//*[@id='billinginfo3_form_phone_code_chosen']/div/div/input")
    WebElement phoneinput;

    @FindBy(id = "billinginfo3-form-phone")
    WebElement phonesecondfield;

    @FindBy(id = "billinginfo3-form-postalcode")
    WebElement zipcodefield;

    @FindBy(xpath = ".//*[@id='billinginfo3_form_stateiso2_chosen']/a/div")
    WebElement statefield;

    @FindBy(xpath = ".//*[@id='billinginfo3-form-cityname']")
    WebElement cityfield;

    @FindBy(xpath=".//*[contains(@class,'signin-switch-account')]")
    WebElement swithaccount;

    @FindBy(xpath = ".//*[contains(@id,'signin3-form-email')]")
    WebElement emailfield;

    @FindBy(id ="signin3-new-customer")
    WebElement continuebuton;

    @FindBy(id ="billinginfo3-form-submit")
    WebElement submitbutton;

    public void registerAccount() {
        GenerateEmail generateEmail =new GenerateEmail();
        MyWaits myWaits = new MyWaits(driver);
        String name = Userold.getDataProperties("name");
        String country = Userold.getDataProperties("country");
        String city = Userold.getDataProperties("city");
        String phone1 = Userold.getDataProperties("phonenumber1");
        String phone2= Userold.getDataProperties("phonenumber2");
        String zipcode= Userold.getDataProperties("zipcode");
        String state= Userold.getDataProperties("state");
         try {
            name = Userold.getDataProperties("name");
            country= Userold.getDataProperties("country");
            city= Userold.getDataProperties("city");
            phone1= Userold.getDataProperties("phonenumber1");
            phone2= Userold.getDataProperties("phonenumber2");
            zipcode= Userold.getDataProperties("zipcode");
        } catch (Exception e) {
            e.printStackTrace();
        }
        myWaits.waitEmail();
        if (swithaccount.isDisplayed()) {
            swithaccount.click();
        }
        emailfield.sendKeys(generateEmail.generateEmail());
        //emailfield.submit();
        continuebuton.click();
        myWaits.waitregForm();
        namefield.sendKeys(name);
        namefield.click();
        countryfield.click();
        countryinput.sendKeys(country);
        countryinput.click();
        dropdownphone.click();
        phoneinput.sendKeys(phone1);
        phoneinput.click();
        phonesecondfield.sendKeys(phone2);
        phonesecondfield.click();
        zipcodefield.sendKeys(zipcode);
        zipcodefield.click();
        if (countryfield.getText().equals("United States")){
            statefield.sendKeys(state);
            statefield.click();
        }
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        cityfield.click();
        cityfield.sendKeys(city);
       cityfield.click();
        submitbutton.click();
    }}
